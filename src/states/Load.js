import firebase from 'firebase/app';
export default class Load extends Phaser.State {
    constructor() {
        super();
    }
    init() {
        this.add.text(this.world.centerX, this.world.centerY, 'Loading...', { font: '25px', fill: '#ddd' }).anchor.set(0.5);
        this.game.hp = 0;
        this.game.level = 1;
        this.game.mode = 1;
        this.game.record = 1;
        this.game.counter = 0;
        this.game.leaders = [];
        this.game.leadersText = [];
        this.game.boardNotReady = true;
        firebase.database().ref('/board').orderByChild('/level').limitToLast(5).on('value', data => {
            this.game.leaders.splice(0);
            data.forEach(data => {
                this.game.leaders.unshift(data.val())
            });
            this.game.boardNotReady = false;
        });
    }
    preload() {
        this.load.baseURL = 'src/assets/';
        this.load.image('background1', 'background1.png');
        this.load.image('background2', 'background2.png');
        this.load.image('texture', 'texture.png');
        this.load.image('menu', 'menu.png');
        this.load.image('leaderboard', 'leaderboard.png');
        this.load.image('start', 'start.png');
        this.load.image('hp', 'hp.png');
        for (let i = 0; i <= 9; ++i) {
            this.load.image(String(i), String(i) + '.png');
            this.load.image('0' + i, '0' + i + '.png');
        }
        this.load.image('button', 'button.png');
        this.load.image('ceiling', 'ceiling.png');
        this.load.image('wall', 'wall.png');
        this.load.image('normal', 'normal.png');
        this.load.image('nails', 'nails.png');
        this.load.spritesheet('conveyorRight', 'conveyor_right.png', 96, 16);
        this.load.spritesheet('conveyorLeft', 'conveyor_left.png', 96, 16);
        this.load.spritesheet('trampoline', 'trampoline.png', 96, 22);
        this.load.spritesheet('fake', 'fake.png', 96, 36);
        this.load.spritesheet('player', 'player.png', 32, 32);

        this.load.image('setting', 'setting.png');
        this.load.image('setting0', 'setting0.png');
        this.load.image('setting1', 'setting1.png');
        this.load.image('setting2', 'setting2.png');
        this.load.image('setting3', 'setting3.png');
        this.load.image('settingcheck', 'settingcheck.png');

        this.load.image('mode1', 'mode1.png');
        this.load.image('mode2', 'mode2.png');
        this.load.image('mode3', 'mode3.png');

        this.load.audio('bgm', 'hipdipper.ogg')
    }
    create() {
        this.add.audio('bgm', 0.3, 1).play();
        this.state.start('Menu');
    }
}