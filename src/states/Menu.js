export default class Menu extends Phaser.State {
    constructor() {
        super();
    }
    create() {
        this.game.createMask();
        this.game.createBackground(1);
        this.game.createWalls();
        this.game.createHealthbar();
        this.game.createLevelText();
        this.game.createPlatforms();
        this.game.createCeiling();
        this.game.createModes();
        this.game.createRecord();
        this.createMenu();
        this.createSetting();
    }
    createMenu() {
        this.menu = this.add.image(this.world.centerX, 105, 'menu');
        this.menu.anchor.set(0.5, 0);

        this.leaderboard = this.add.button(this.world.centerX, 327, 'leaderboard', () => {
            if (this.game.boardNotReady) {
                return;
            }
            if (!this.game.board) {
                this.game.board = this.add.graphics();
                this.game.board.beginFill(0, 1);
                this.game.board.drawRect(175, 120, 282, 140);
                this.game.board.endFill();
            } else {
                this.game.board.destroy();
                this.game.board = undefined;
            }
            if (!this.game.leadersText.length) {
                const style = { font: '14px', fill: 'yellow' };
                for (let i = 0; i < this.game.leaders.length; ++i) {
                    this.game.leadersText.push(this.add.text(190, 130 + 25 * i, i + 1 + '.', style));
                    this.game.leadersText.push(this.add.text(210, 130 + 25 * i, this.game.leaders[i].name, style));
                    const t = this.add.text(450, 130 + 25 * i, `地下 ${this.game.leaders[i].level} 階`, style);
                    t.anchor.set(1, 0);
                    this.game.leadersText.push(t);
                }
            } else {
                for (let leaderText of this.game.leadersText) {
                    leaderText.destroy();
                }
                this.game.leadersText.splice(0);
            }
        }, this);
        this.leaderboard.anchor.set(0.5, 0);

        this.start = this.add.button(this.world.centerX, 367, 'start', () => {
            this.state.start('Main');
        }, this);
        this.start.anchor.set(0.5, 0);
    }
    createSetting() {
        this.setting = this.add.image(53, 42, 'setting');
        this.setting.visible = false;

        this.setting0 = this.add.button(53, 23, 'setting0', () => {
            this.setting.visible = true;
            this.setting0.alpha = 1;
            this.setting1.visible = true;
            this.setting2.visible = true;
            this.setting3.visible = true;
            this.settingcheck.visible = true;
        }, this);
        this.setting0.alpha = 0;

        this.setting1 = this.add.button(56, 45, 'setting1', () => {
            this.game.mode = 1;
            this.settingcheck.y = 51 + (this.game.mode - 1) * 20;
            this.game.updateModes();
        }, this);
        this.setting2 = this.add.button(56, 65, 'setting2', () => {
            this.game.mode = 2;
            this.settingcheck.y = 51 + (this.game.mode - 1) * 20;
            this.game.updateModes();
        }, this);
        this.setting3 = this.add.button(56, 85, 'setting3', () => {
            this.game.mode = 3;
            this.settingcheck.y = 51 + (this.game.mode - 1) * 20;
            this.game.updateModes();
        }, this);
        this.setting1.visible = false;
        this.setting2.visible = false;
        this.setting3.visible = false;
        this.setting1.alpha = 0;
        this.setting2.alpha = 0;
        this.setting3.alpha = 0;

        this.setting1.onInputOver.add(() => {
            this.setting1.alpha = 1;
            this.setting2.alpha = 0;
            this.setting3.alpha = 0;
        }, this);
        this.setting2.onInputOver.add(() => {
            this.setting1.alpha = 0;
            this.setting2.alpha = 1;
            this.setting3.alpha = 0;
        }, this);
        this.setting3.onInputOver.add(() => {
            this.setting1.alpha = 0;
            this.setting2.alpha = 0;
            this.setting3.alpha = 1;
        }, this);

        this.settingcheck = this.add.image(59, 51 + (this.game.mode - 1) * 20, 'settingcheck');
        this.settingcheck.visible = false;
    }
    update() {
        this.game.updateWall();
        this.game.updateTexture();
        this.game.updateHealthbar();
        this.game.updatePlatforms(true);
        const pointer = this.input.activePointer;
        if (pointer.isDown) {
            this.setting.visible = false;
            this.setting0.alpha = 0;
            this.setting1.visible = false;
            this.setting2.visible = false;
            this.setting3.visible = false;
            this.settingcheck.visible = false;
        }
    }
}