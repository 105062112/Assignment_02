import Load from 'states/Load.js';
import Menu from 'states/Menu.js';
import Main from 'states/Main.js';

export default class Game extends Phaser.Game {
    constructor(x, y) {
        super(x, y);
        this.state.add('Load', Load);
        this.state.add('Menu', Menu);
        this.state.add('Main', Main);
        this.state.start('Load');
    }
    createMask() {
        this.mask = this.add.graphics();
        this.mask.beginFill();
        this.mask.drawRect(0, 105, 635, 355);
        this.mask.endFill();
    }
    createBackground(n) {
        this.background = this.add.image(0, 0, 'background' + n);
        this.texture = this.add.image(24, 105, 'texture');
        this.texture.mask = this.mask;
        this.texture.width = 418;
        this.texture.height = 1524 * 418 / 768;
    }
    createCeiling() {
        this.ceiling = this.add.image(43, 105, 'ceiling');
    }
    createWalls() {
        this.walls = this.add.group();
        this.walls.mask = this.mask;
        this.walls.enableBody = true;
        this.add.sprite(24, 105, 'wall', 0, this.walls);
        this.add.sprite(424, 105, 'wall', 0, this.walls);
        this.walls.setAll('body.immovable', true);
    }
    createHealthbar() {
        this.healthbar = this.add.group();
        this.healthbar.x = 49;
        this.healthbar.y = 75;
        for (let i = 0; i < 12; ++i) {
            this.add.image(i * 8, 0, 'hp', 0, this.healthbar);
        }
    }
    createLevelText() {
        this.levelText = this.add.group();
        this.levelText.mask = this.add.graphics();
        this.levelText.mask.beginFill();
        this.levelText.mask.drawRect(0, 57, 635, 62);
        this.levelText.mask.endFill();
        this.levelText.x = 266;
        this.levelText.y = 57;
        this.add.image(29 * 0, 0, String(~~(this.level / 1000) % 10), 0, this.levelText);
        this.add.image(29 * 1, 0, String(~~(this.level / 100) % 10), 0, this.levelText);
        this.add.image(29 * 2, 0, String(~~(this.level / 10) % 10), 0, this.levelText);
        this.add.image(29 * 3, 0, String(this.level % 10), 0, this.levelText);
    }
    createPlatforms() {
        this.platforms = this.add.group();
        this.platforms.mask = this.mask;
        this.platforms.enableBody = true;
        this.createPlatform(Math.random() * 290 + 40, 450 - 65 * 4);
        this.createPlatform(Math.random() * 290 + 40, 450 - 65 * 3);
        this.createPlatform(Math.random() * 290 + 40, 450 - 65 * 2);
        this.createPlatform(Math.random() * 290 + 40, 450 - 65 * 1).body.enable = false;
        this.createPlatform(185, 450, 'normal');
        this.platforms.setAll('body.immovable', true);
    }
    createPlatform(x, y, type) {
        const rand = Math.random();
        let platform;
        if (type === 'normal' || rand < 0.5) {
            platform = this.add.sprite(x, y, 'normal');
        } else if (rand < 0.6) {
            platform = this.add.sprite(x, y, 'nails');
            this.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 0.7) {
            platform = this.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 0.8) {
            platform = this.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 0.9) {
            platform = this.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = this.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }
        this.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
        this.platforms.add(platform);

        return platform;
    }
    createModes() {
        this.modes = this.add.group();
        this.modes.y = 152;
        this.mode1 = this.add.image(627, 0, 'mode1', 0, this.modes);
        this.mode2 = this.add.image(627, 0, 'mode2', 0, this.modes);
        this.mode3 = this.add.image(627, 0, 'mode3', 0, this.modes);
        this.modes.setAll('anchor', new Phaser.Point(1, 0));
        this.updateModes();
    }
    createRecord() {
        this.recordText = this.add.group();
        this.recordText.x = 545;
        this.recordText.y = 207;
        this.add.image(12 * 0, 0, '0' + ~~(this.record / 1000) % 10, 0, this.recordText);
        this.add.image(12 * 1, 0, '0' + ~~(this.record / 100) % 10, 0, this.recordText);
        this.add.image(12 * 2, 0, '0' + ~~(this.record / 10) % 10, 0, this.recordText);
        this.add.image(12 * 3, 0, '0' + this.record % 10, 0, this.recordText);
    }
    updateWall() {
        if (--this.walls.y < -33) {
            this.walls.y = 0;
        }
    }
    updateTexture() {
        if ((this.texture.y -= 0.5) < 105 - 254 * 418 / 768) {
            this.texture.y = 105;
        }
    }
    updateHealthbar() {
        for (let i = 0; i < 12; ++i) {
            this.healthbar.getAt(i).alpha = (i < this.hp ? 1 : 0);
        }
    }
    updatePlatforms(fromMenu) {
        if (fromMenu) {
            this.platforms.addAll('y', -1);
        } else {
            this.platforms.addAll('y', -(this.mode / 2 + 0.5));
        }
        if (this.platforms.getTop().y === 414) {
            this.createPlatform(Math.random() * 290 + 40, 480);
            ++this.counter;
            if (this.counter % 5 === 0) {
                if (!fromMenu) {
                    this.updateLevel();
                }
            }
        }
    }
    updateLevel() {
        ++this.level;
        if (this.level % 1000 === 0) {
            this.add.image(29 * 0, 34, String((this.level / 1000) % 10), 0, this.levelText);
            this.updatingDigit0 = true;
        }
        if (this.level % 100 === 0) {
            this.add.image(29 * 1, 34, String((this.level / 100) % 10), 0, this.levelText);
            this.updatingDigit1 = true;
        }
        if (this.level % 10 === 0) {
            this.add.image(29 * 2, 34, String((this.level / 10) % 10), 0, this.levelText);
            this.updatingDigit2 = true;
        }
        this.add.image(29 * 3, 34, String(this.level % 10), 0, this.levelText);
        this.updatingDigit3 = true;
    }
    updateDigitAnimation() {
        if (this.updatingDigit0) {
            this.levelText.addAll('y', -2);
            if (this.levelText.getChildAt(4).y === 0) {
                this.levelText.removeBetween(0, 3);
                this.updatingDigit3 = false;
                this.updatingDigit2 = false;
                this.updatingDigit1 = false;
                this.updatingDigit0 = false;
            }
        } else if (this.updatingDigit1) {
            for (let i = 1; i <= 6; ++i) {
                this.levelText.getChildAt(i).y -= 2;
            }
            if (this.levelText.getChildAt(4).y === 0) {
                this.levelText.removeBetween(1, 3);
                this.updatingDigit3 = false;
                this.updatingDigit2 = false;
                this.updatingDigit1 = false;
            }
        } else if (this.updatingDigit2) {
            for (let i = 2; i <= 5; ++i) {
                this.levelText.getChildAt(i).y -= 2;
            }
            if (this.levelText.getChildAt(4).y === 0) {
                this.levelText.removeBetween(2, 3);
                this.updatingDigit3 = false;
                this.updatingDigit2 = false;
            }
        } else if (this.updatingDigit3) {
            for (let i = 3; i <= 4; ++i) {
                this.levelText.getChildAt(i).y -= 2;
            }
            if (this.levelText.getChildAt(4).y === 0) {
                this.levelText.removeChildAt(3);
                this.updatingDigit3 = false;
            }
        }
    }
    updateModes() {
        this.modes.setAll('visible', false);
        if (this.mode === 1) {
            this.mode1.visible = true;
        } else if (this.mode === 2) {
            this.mode2.visible = true;
        } else if (this.mode === 3) {
            this.mode3.visible = true;
        }
    }
}