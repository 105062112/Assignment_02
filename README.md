# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## REPORT

1. complete game process
2. follow the basic rules of  "小朋友下樓梯"
3. correct physical properties and behaviors
4. at least 2 kinds of platform (做了六種)
5. sound fx （bgm） / UI (鍵盤或觸控來操作角色）（暫停和停止的按鈕)（更改難度的選單）
6. leaderboard with firebase
FEATURE：對原版遊戲極高的還原度，包括：牆壁與平台一起向上移動、背景以較慢速度移動（背景圖還得用原版遊戲截圖之後修掉平台得到）、角色移動速度、重力的大小、碰到不同機關的行為、右上角階數上升的動畫、數字的樣式、整體的介面，都花很多時間研究原版遊戲來琢磨。

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed
